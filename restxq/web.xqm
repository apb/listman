(:~ 
:  web application utilities
: @author andy bunce
: @since apr 2012
:)

module namespace web = 'apb.web.utils';
declare default function namespace 'apb.web.utils'; 

declare namespace rest = 'http://exquery.org/ns/restxq';
declare namespace xf = 'http://www.w3.org/2002/xforms';
declare namespace xhtml = 'http://www.w3.org/1999/xhtml';
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";

 

(:~
: xsltforms processing-instructions
:)
declare variable $web:forms-pi:=(
     processing-instruction{"xml-stylesheet"} {'href="/lib/xsltforms/xsltforms.xsl" type="text/xsl"'},
     processing-instruction{"css-conversion"} {'no'},
     processing-instruction{"xsltforms-options"} {'debug="no"'}
);

(:~
: update template from map
: @return updated doc with xsltforms processing instructions
:)
declare function layout($template,$map) {
    copy $page := $template
    modify (
       for $m in map:keys($map)
       let $v:=$map($m)
       return

       if("title"=$m)
       then   replace value of node  $page//*[@id="title"] with $v
       
       else if("model"=$m)
       then   insert node $v into $page//*[@id="head"] 
        
       else if("content"=$m)
       then   insert node $v into  $page//*[@id="content"]
       
	   else if("sidebar"=$m)
       then   insert node $v into  $page//*[@id="sidebar"]      	   
       else ()
        )
      return document { if(map:contains($map, "model"))
                        then $web:forms-pi
                        else ()
                       ,$page}     
};

(:~
: updating version of layout
:)
declare updating function output($template,$map) {
 db:output(layout($template,$map))
};

(:~
: show an image as a 404
: Adam's 'restxq examples has binary not raw
:)
declare function page404($image){
  <rest:response>
           <output:serialization-parameters>
                <output:method value="raw"/>
           </output:serialization-parameters>
           <http:response status="404" reason="not found">
           
              <http:header name="Content-Type" value="image/jpeg"/>
              
           </http:response>
       </rest:response>,
      file:read-binary($image)
};

(:~
: redirect to ..
:)
declare function redirect($url as xs:string) 
 {
        <rest:response>         
           <http:response status="301" >
             <http:header name="Location" value="{$url}"/>
           </http:response>                      
       </rest:response>
};