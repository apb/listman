(:~ 
: A listmanager  web appication using REST-annotations
: @author andy bunce
: @since apr 2012
:)

module namespace sr = 'apb.listman.rest';
declare default function namespace 'apb.listman.rest'; 
import module namespace web = 'apb.web.utils' at "restlib/web.xqm";
import module namespace led = 'apb.listman.edit' at "listman/edit.xqm";
import module namespace test = 'apb.xform.test' at "listman/xform-test.xqm";

declare namespace rest = 'http://exquery.org/ns/restxq';
declare namespace xf = 'http://www.w3.org/2002/xforms';
declare namespace xhtml = 'http://www.w3.org/1999/xhtml';

declare variable $sr:collection := 'listman';



declare variable $sr:layout:=<html
xmlns="http://www.w3.org/1999/xhtml"
    xmlns:xf="http://www.w3.org/2002/xforms" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:ev="http://www.w3.org/2001/xml-events"
>
 <head id="head">
  <meta charset="utf-8"/>
 <title id="title">Listman v0.5.0</title>
 <base href="/restxq/listman/" />
 <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="description" content="xml item manager"/>
  <meta name="author" content="andy bunce"/>
 <link href="/lib/bootstrap/2.0.4/css/bootstrap.css" rel="stylesheet" type="text/css" />
 <link href="/lib/bootstrap/2.0.4/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
 <link href="/listman/app.css" rel="stylesheet" type="text/css" />

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="/listman/favicon.ico" />
<style type="text/css">
    <![CDATA[
@namespace xf url("http://www.w3.org/2002/xforms");

/* Color current item yellow. */
.xforms-repeat-item-selected {background-color: yellow;} 

/* Within an invalid control, color the input field pink. */
.xforms-invalid .xforms-value {background-color: pink ;}

.block-form xf|label {
          width: 15ex;
}
xf|label {font-weight: bold;}
 
.xforms-value {
  width:25ex;background-color: #FAFAD2;
}
.description .xforms-value {
  width:50ex;
}
   ]]>
</style>
  <script src="/lib/jquery/jquery-1.7.1.min.js" type="text/javascript"></script>
  <script src="/lib/bootstrap/2.0.4/js/bootstrap.js" type="text/javascript"></script>

 </head>
 <body style="margin-top:44px;">
 <div class="navbar navbar-fixed-top " data-dropdown="dropdown">
        <div class="navbar-inner">
            <div class="container-fluid">
            
                <a href="." class="brand">
                <img src="/listman/listman.png"/>
                Listman</a>
                <ul class="nav">
               <li><a href="list">List</a></li>
			   <li><a href="edit?new=true">New</a></li>
			   <li class="divider-vertical"></li>
			   <li><a href="categories">Categories</a></li>
			   <li><a href="tags">Tags</a></li>
                </ul>
               <form class="navbar-search" action="search">
                    <input type="text" class="search-query span3" placeholder="Search"
                        name="q" />
                    <div class="icon-search"></div>
                </form>
                <ul class="nav pull-right">
                    <li><a href="about">About</a></li>
                    <li class="divider-vertical"></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="next">next-id test</a></li>
                        <li><a href="test">XForms test</a></li>
                        <li><a href="test404">404 test</a></li>
                         <li><a href="session">session</a></li>
                        <li class="divider"></li>
                        <li><a href="source">source</a></li>
                      </ul>
                    </li>
              </ul>
            </div>
        </div>
        </div>
<div class="container-fluid" >
<div class="row-fluid">   
    <div id="content" class="span9" ></div>
     <div id="sidebar" class="span2 well">This sidebar will soon have
     something more interesting.</div>
</div>
  
  </div>
  <footer class="footer">
        <div class="container">
            <hr />
                <ul class="pull-right nav nav-pills">
              <li>
            <a href="https://bitbucket.org/apb/listman" target="_blank">Code (bitbucket)</a>
            </li>
            <li>
            <a href="http://cubeb.blogspot.co.uk/search/label/listman" target="_blank">Blog</a>
            </li>
            </ul>
        </div>
  </footer>
  <!--
  <script type="text/javascript"><![CDATA[
        var _gaq = _gaq || [];
        _gaq.push([ '_setAccount', 'UA-28597688-1' ]);
        _gaq.push([ '_trackPageview' ]);

        (function() {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl'
                    : 'http://www')
                    + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
        
     ]]></script>
     -->
 </body>
</html>;

declare %rest:path("listman") %output:method("html5")
function root() {
 let $x:= db-create()
 let $t:=    <div class="hero-unit">
    <h1>Listman</h1>
    <p>Manage a set of XML data files</p>
    <p>
    <a class="btn btn-primary btn-large" href="list">
    Show items
    </a>
    </p>
    </div>
 return web:layout($sr:layout,map{"content":= $t,"sidebar":=$x})
};

declare %rest:path("listman/search") %output:method("html5")
%rest:form-param("q", "{$q}")
function search($q ) {

 let $t:=<div>
    <h1>Search Results</h1>
        <p><b>Search results for: </b>{$q} <b> In collection: </b>{$sr:collection}</p>
     <ol>{
        (: search any column that matchs this string :)
        for $hit in fn:collection($sr:collection)/item
        where some $t in $hit/*/text() satisfies fn:contains($t,$q)
        return
           <li><a href="view?id={$hit/id/text()}">{$hit/name/text()}</a></li>
      }</ol>
      
      
      <a href="edit?new=true">New Item</a>
      <a href="list">List Items</a>
      
       <div class="pagination">
       Not working..
    <ul>
    <li><a href="#">Prev</a></li>
    <li class="active">
    <a href="#">1</a>
    </li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">Next</a></li>
    </ul>
    </div>
    </div>	  
 return web:layout($sr:layout,map:new(map:entry("content", $t)))
};

declare %rest:path("listman/about") %rest:GET
%output:method("html5")
function about() {
 let $t:=<div>
 <p>This application maintains a set of XML files via a web interface. It uses the 
 <a href="http://basex.org"  target="_blank">BaseX</a> 
<a href="http://docs.basex.org/wiki/RESTXQ"  target="_blank">RestXQ</a>
 implementation which allows the use of XQuery annotations to 
specify web server behavior. </p>
<p>
<a href="http://www.agencexml.com/xsltforms"  target="_blank">XSLTForms</a>
  , an XForms implementation, is used for editing. </p>
<p>This application is based on Dan Mccreary's Item manager from the 
<a href="http://en.wikibooks.org/wiki/XRX"  target="_blank">XRX wikibook</a>.
The original code is
<a href="http://code.google.com/p/xrx/source/browse/#svn%2Ftrunk%2F06-item-manager" target="_blank">here</a>.
</p>
<p>Twitter 
<a href="http://twitter.github.com/bootstrap/index.html"  target="_blank">bootstrap</a> is used for the client side styling.
</p>
</div>
 return web:layout($sr:layout,map:new(map:entry("content", $t)))
};

declare %rest:path("listman/list") %rest:GET
%output:method("html5")
function list() {
 let $t:= view-list()   
 return web:layout($sr:layout,map:new(map:entry("content", $t)))
};

(:~
: test session
:) 
declare %rest:path("listman/session") %rest:GET
%output:method("html5")
%rest:cookie-param("sid","{$sid}","no")
function session($sid) {
 let $t:= if($sid)
          then <div>{ "Session id: " || $sid}</div>
          else <div>No session</div>
 return web:layout($sr:layout,map:new(map:entry("content", $t)))
};

(:~
: test session
:) 
declare %rest:path("listman/source") %rest:GET
%output:method("html5")
function source() {
 let $t:=  <pre>{ fn:unparsed-text(fn:static-base-uri()) }</pre>
 return web:layout($sr:layout,map:new(map:entry("content", $t)))
};
 
(:~
: test 404 and image o/p
:)
declare %rest:path("listman/test404") %rest:GET
function test404() {
 let $f:=fn:resolve-uri("listman/404.jpg",fn:static-base-uri())
 return web:page404($f)
};

declare %rest:path("listman/next") %rest:GET
%output:method("html5")
updating function next() {
	let $id:=$led:next-id/data/next-id/text()+1
	let $xml:=<item><id>{$id}</id>
	            <msg>This is a test of id increment, no file saved.</msg>
	           </item>
	return (led:incr-id(),
		   output(show-save($xml))
	  )
};

declare function show-save($xml){
    let $id:=$xml/item/id/text()
    return
    <div>
        <p>Item #{$id}# has been saved.</p>
          <pre>{fn:serialize($xml)}</pre>
        <a href="list">List all Items</a> 
    </div>
};

(:~
: posting to /list will update or create an item
: if an id element is present the associated file is updated
: otherwise a new item is created and the id counter incremented 
:)
declare %rest:path("listman/list") %rest:POST("{$post-body}")
%output:method("html5")
updating function list-post($post-body) {
    let $new:=fn:empty($post-body/item/id/text())
    let $fix:=led:ensure-id($post-body)
    return (if($new) then led:incr-id()else()
           ,led:save3($new,$fix)
           ,output(show-save($fix))
           )      
};

declare %rest:path("listman/list/{$id}") %output:method("xml")
function list-item($id) {
    db:open($sr:collection,$id)
};

declare %rest:path("listman/view") %output:method("html5")
%rest:form-param("id", "{$id}")
function view($id ) {
 let $t:=if($id)
		 then view-item($id)
		 else <div>Parameter "id" is missing.  This argument is required.</div>
					 
 return web:layout($sr:layout,map:new(map:entry("content", $t)))
};

declare %rest:path("listman/delete-confirm") %output:method("html5")
%rest:form-param("id", "{$id}")
function delete-confirm($id ) {
 let $t:=if($id)
		 then delete-confirm-div($id)
		 else <div>Parameter "id" is missing.  This argument is required.</div>
					 
 return web:layout($sr:layout,map:new(map:entry("content", $t)))
};

declare %rest:path("listman/delete") %output:method("html5")
%rest:form-param("id", "{$id}")
updating function delete($id ) {
     if($id)
     then let $d:=<div>deleted</div>
          return (delete-do($id),output($d))
     else output(<div>Parameter "id" is missing.  This argument is required.</div>)
};

declare updating function delete-do($id) {
 let $fname:=$id || ".xml"
 return db:delete("listman",$fname )
};

declare %rest:path("listman/edit") %output:method("xml")
%rest:form-param("id", "{$id}")
function edit($id) {
    let $t:=led:edit($id)
    return web:layout($sr:layout,$t)
};

declare %rest:path("listman/categories") %output:method("html5")
function categories() {
	web:layout($sr:layout,map:new(
		map:entry("content", <div>Categories coming soon.</div>),
	  map:entry("sidebar", <p>A bit more sidebar content.</p>)))  
};

declare %rest:path("listman/test") %output:method("xml")
function test() {
 web:layout($sr:layout,test:test())
};
  
declare %rest:path("listman/tags") %output:method("html5")
function tags() {
 let $s:=<p>Yes, really 
                  <span class="label label-info">soon</span> now.
         </p>
 let $c:=    <div class="page-header">
    <h1>All about tags</h1>
    Tags coming soon.
    </div>        
 return        
 web:layout($sr:layout,map:new((
		map:entry("title", "All about tags"),
    map:entry("content", $c),
    map:entry("sidebar", $s)
		))
	)  
};
      
declare function delete-confirm-div($id){
    let $item := fn:collection($sr:collection)/item[id = $id]
    let $t:=<div>
     <a href=".">Item Home</a> &gt; <a href="list">List Items</a>
     <div class="hero-unit">
            <h1>Are you sure you want to delete this Item?</h1>
            <b>Name: </b>{$item/name/text()}<br/>       
            <a class="btn btn-danger" href="delete?id={$id}">Yes - Delete This Item</a>
             
             <a  class="btn" href="view?id={$id}">Cancel (Back to View Item)</a>
    		 </div>
      </div>
	return $t	 
};

declare function view-list() {
(    <div class="page-header">
    <h1>List of all items</h1>
    </div>
,
<table class="table table-condensed table-striped ret">
       <thead>
       <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Category</th>
          <th>Status</th>
           <th>Tag</th>
          <th>View</th>
          <th>Edit</th>
          <th>Delete</th>
       </tr>
    </thead>
    <tbody>{
      for $item in fn:collection($sr:collection)/item[id/text()]
         let $id := $item/id/text()
		  
      return
         <tr>
             <td><span class="badge">{$id}</span></td>
             <td>{$item/name/text()}</td>
             <td>{$item/category/text()}</td>
             <td>{$item/status/text()}</td>
              <td>{$item/tag/text()}</td>
             <td><a class="btn btn-mini btn-info" href="view?id={$id}">View</a></td>
             <td><a class="btn btn-mini btn-primary" href="edit?id={$id}">Edit</a></td>
             <td><a class="btn btn-mini btn-danger" href="delete-confirm?id={$id}">
			 <i class="icon-remove"></i> Delete</a></td>
         </tr> 
   }</tbody>
</table>
)
};

declare function view-item($id) {
  (
 <div class="form-actions">
   <h1>View Item</h1>
   <a class="btn btn-primary" href="edit?id={$id}">Edit Item</a>    
   <a class="btn btn-danger" href="delete-confirm?id={$id}">Delete Item</a>
   </div>
   ,
   let $item := fn:collection($sr:collection)/item[id = $id]
      return
         <table>
            <tbody>
             <tr><th>ID:</th><td><a href="list/{$id}.xml">{
             $item/id/text()
             }</a></td></tr>
             <tr><th>Name:</th><td>{$item/name/text()}</td></tr>
             <tr><th>Description:</th><td>{$item/description/text()}</td></tr>
             <tr><th>Category:</th><td>{$item/category/text()}</td></tr>
             <tr><th>Status:</th><td>{$item/status/text()}</td></tr>
             <tr><th>Tag:</th><td>{$item/tag/text()}</td></tr>
             </tbody>
          </table>
   
   )
};

declare updating function output($content){
    web:output($sr:layout, map:new(map:entry("content", $content)))
};

(:~ ensure db exists
:) 
declare function db-create(){
    let $c := client:connect('localhost', 1984, 'admin', 'admin')
    let $path:=fn:resolve-uri("listman/data")
    let $path:=file:path-to-native($path)
   
    let $cmd:="CREATE DB " || $sr:collection || " " || $path  
    return 

        if (db:exists($sr:collection))
        then "db found"
        else  (client:execute($c, "SET CHOP FALSE"), 
               client:execute($c, $cmd),
               "Database created") 
};
