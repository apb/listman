(:~ 
:  xform item edit functions
: @author andy bunce
: @since apr 2012
:)

module namespace led = 'apb.listman.edit';
declare default function namespace 'apb.listman.edit'; 

declare namespace rest = 'http://exquery.org/ns/restxq';
declare namespace xf = 'http://www.w3.org/2002/xforms';
declare namespace ev = 'http://www.w3.org/2001/xml-events';
declare namespace xhtml = 'http://www.w3.org/1999/xhtml';
declare variable $led:collection := 'listman';
declare variable $led:next-id :=db:open($led:collection,"next-id.xml");

(:~
: return map with xforms model and ui to edit $id
:)
declare function edit($id) {
    let $file:= filename($id)
    return map{
    "model":=
           <xf:model>
               <!-- this line loads either the new instance or the current data file into the form model -->
               <xf:instance xmlns="" src="{$file}" />
               <xf:bind id="xname" nodeset="/item/name" required="true()"/>
               <xf:submission id="save" method="post" action="list" instance="my-task" 
               replace="all" >
			    <xf:message ev:event="xforms-submit-error"
				>A submission error occurred.
				(<xf:output value="event('error-type')"/>)
                                    
				</xf:message>
			   </xf:submission>
             

           </xf:model>
    ,"content":=
      <div>
       <h2>Edit Item</h2>
           <xf:submit submission="save" class="btn btn-mini">
               <xf:label>Save</xf:label>
           </xf:submit>
       
           <xf:output ref="id" class="id">
               <xf:label>ID:</xf:label>
                <xf:hint>The id is automatically assigned and can 
                not be set or changed.</xf:hint>
           </xf:output>
            
           
           <xf:input ref="name">
               <xf:label>Name:</xf:label>
               <xf:alert>A name must be supplied.</xf:alert>
           </xf:input>
           
           <xf:textarea ref="description" class="description">
               <xf:label>Question:</xf:label>
               <xf:hint>What is this about etc.</xf:hint>
           </xf:textarea>
           
           <xf:select1 ref="category"  class="category">
           <xf:label>Category:</xf:label>
                   <xf:item>
                      <xf:label>Small</xf:label>
                      <xf:value>small</xf:value>
                   </xf:item>
                   <xf:item>
                      <xf:label>Medium</xf:label>
                      <xf:value>medium</xf:value>
                   </xf:item>
                   <xf:item>
                      <xf:label>Large</xf:label>
                      <xf:value>large</xf:value>
                   </xf:item>
           </xf:select1>
           
           <xf:select1 ref="status"  class="status">
           <xf:label>Status:</xf:label>
                   <xf:item>
                      <xf:label>Draft</xf:label>
                      <xf:value>draft</xf:value>
                   </xf:item>
                   <xf:item>
                      <xf:label>In Review</xf:label>
                      <xf:value>in-review</xf:value>
                   </xf:item>
                   <xf:item>
                      <xf:label>Approved for Publishing</xf:label>
                      <xf:value>published</xf:value>
                   </xf:item>
                  
           </xf:select1>
           
           <xf:input ref="tag">
               <xf:label>Tag:</xf:label>
                <xf:message level="ephemeral" ev:event="DOMFocusIn">
                This is an ephemeral message.
                But it does NOT go away after a few seconds.</xf:message>
           </xf:input>
         
        
           </div>
    }
};

(:~
: save xml 
:)
declare updating function save3($new,$xml){
    let $id:=$xml/item/id/text()
    let $fname:=$id || ".xml"
    return if($new)
           then db:add("listman",$xml,$fname )
           else db:replace("listman",$fname,$xml )
};

(:~
: increment the file id
:)
declare updating function incr-id(){
    let $x:=$led:next-id/data/next-id/text()+1
	let $u:=copy $n:=$led:next-id
	        modify(
				replace value of node $n/data/next-id with $x
            )
			return $n
    return db:replace( $led:collection,"next-id.xml",$u)      			
};

(:~
: fixup xml before save, ensure id and update timestamp
:)
declare function ensure-id($xml){
   copy $n:=$xml
	modify(
	    if($n/item/id/text())
		then ()
		else replace value of node $n/item/id with $led:next-id/data/next-id/text()+1
	   ,
		if($n/item/@mod)
		then replace value of node $n/item/@mod with  fn:current-dateTime()
		else insert node attribute mod { fn:current-dateTime() } into $n/item 

	)
    return $n	
};

declare %private function filename($id){
    "list/" || (if($id) then $id || ".xml"  else "0.xml")
};