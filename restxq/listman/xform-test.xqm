(:~ 
: test xform function
: @author andy bunce
: @since apr 2012
:)

module namespace test = 'apb.xform.test';
declare default function namespace 'apb.xform.test'; 

declare namespace rest = 'http://exquery.org/ns/restxq';
declare namespace xf = 'http://www.w3.org/2002/xforms';
declare namespace ev = 'http://www.w3.org/2001/xml-events';
declare namespace xhtml = 'http://www.w3.org/1999/xhtml';

declare variable $test:model:=<xf:model>
         <xf:instance>
            <data xmlns="">
               <PersonGivenName/>
            </data>
         </xf:instance>
      </xf:model>;

declare function test(){
    map{"content":=xform-test(),"model":=$test:model}
};

declare function xform-test(){
  <div>
   <p>Type your first name in the input box. <br/>
        If you are running XForms, the output should be displayed in the output area.</p>   
     <xf:input ref="PersonGivenName" incremental="true">
            <xf:label>Please enter your first name: </xf:label>
     </xf:input>
     <br />
     <xf:output value="concat('Hello ', PersonGivenName, '. We hope you like XForms!')">
            <xf:label>Output: </xf:label>
     </xf:output>
   </div>
};      