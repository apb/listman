# Listman - A demo for BaseX with XSLTForms and RestXQ 

This applications maintains a set of XML files via a web interface. 
It uses the BaseX [RestXQ](http://docs.basex.org/wiki/RESTXQ) implementation
 which allows the use of XQuery annotations to specify web server behavior. 
[XSLTForms](http://www.agencexml.com/xsltforms) is used for editing. 
This application is based on Dan Mccreary's Item manager from the 
[XRX wikibook](http://en.wikibooks.org/wiki/XRX) 
[code](http://code.google.com/p/xrx/source/browse/#svn%2Ftrunk%2F06-item-manager)


## Installation

1. Install [BaseX](http://basex.org/products/download/all-downloads/) Version 7.3.1 or greater is required
2. Download the third party dependencies [lib-web](https://github.com/downloads/apb2006/xqwebdoc/web-libraries.zip) and unzip to
the BaseX http server folder.
The folder structure should look like:

````
	..basex/http/lib/xsltforms/..
	              /bootstrap/..   
	              /js/..
````           
3. Download the listman project [listman](https://bitbucket.org/apb/listman)  copy the contents of the restxq 
 folder to the http folder.
 The structure should now look like

````
	 ../basex/http/lib/..
	         /listman/..
	         listman.xqm
````
 
## To run
 
1. Start the basex http server basexhttp
2. Navigate to http://localhost:8984/restxq/listman/
3. Enter the account and password defaults are "admin" and "admin"

## History

1. version 0.5.0 update to basex 7.3.1, bootstrap to 2.0.4, create db if not found
2. version 0.4.1 initial release